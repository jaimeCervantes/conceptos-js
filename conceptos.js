if(0) {
	console.log(0)
}

if("") {
	console.log('cadena vacia')
}

if('') {
	console.log('cadena vacia');
}

if(null) {
	console.log(null)
}

if(undefined) {
	console.log(undefined)
}

if(false) {
	console.log(false)
}

if(NaN) {
	console.log(NaN)
}


const objNuevo = {}
if(objNuevo) {
	console.log(objNuevo)
}

const arrNuevo = [];
if (arrNuevo) {
	console.log(arrNuevo)
}

if(-1) {
	console.log(-1)
}